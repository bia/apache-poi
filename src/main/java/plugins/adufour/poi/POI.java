package plugins.adufour.poi;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache POI library. <br>
 * NB: out of convenience, this library includes XML beans 2.6.0 as well, since POI is one of the
 * last libraries to depend heavily on this (deprecated) library.
 * 
 * @author Alexandre Dufour
 */
public class POI extends Plugin implements PluginLibrary
{
    
}
